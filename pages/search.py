from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class DuckDuckGoSearchPage:
    URL = 'https://www.duckduckgo.com'
    SEARCH_FIELD = (By.ID, 'search_form_input_homepage')

    def __init__(self, browser):
        self.browser = browser

    @property
    def load(self):
        self.browser.get(self.URL)

    def search(self, text_to_search):
        search_field = self.browser.find_element(*self.SEARCH_FIELD)
        search_field.send_keys(text_to_search + Keys.RETURN)