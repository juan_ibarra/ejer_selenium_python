import pytest
from selenium.webdriver import Chrome
from pages.search import DuckDuckGoSearchPage
from pages.results import DuckDuckGoResultPage


@pytest.fixture
def browser():
    driver = Chrome()
    driver.implicitly_wait(10)
    driver.maximize_window()
    yield driver
    driver.quit()


def test_basico_busqueda_duckduckgo(browser):
    BUSQUEDA = 'Automation'

    search_page = DuckDuckGoSearchPage(browser)
    search_page.load
    search_page.search(BUSQUEDA)
    result_page = DuckDuckGoResultPage(browser)

    assert result_page.link_div_count() > 0
    assert result_page.phrase_result_count(BUSQUEDA) > 0
    assert result_page.search_input_value() == BUSQUEDA